# Piwigo-Java / Piwigo Remote Sync

**Piwigo Remote Sync** is an **uploader** for the **Piwigo photo** gallery software

* Piwigo-Java is a GitHub project for the Piwigo Remote Sync Java application
* Piwigo Remote Sync is also a reusable API for the Piwigo photo gallery software
* Piwigo Remote Sync is available in **beta** version

# Piwigo Remote Sync Api by FreeElf
This repo is a fork of the original repo [Piwigo-Java](https://github.com/Piwigo/Piwigo-Java).

* A fix of the version 1.0-beta has been made to make command line synchronisation work.
* Originally, it was only possible to synchronise files whose extensions were specified in the code (namely "jpg", "jpeg", "png", "gif", "tif", "tiff"). However, there are plugins that allow piwigo to manage other extensions (notably videos). A new optional argument *-addext* to add file extensions to be synchronised has been added.

# Operation and example of the Piwigo Remote Sync Api by FreeElf
First, compile the folder *remotesync-api* to obtain the jar *remotesync.jar*. By default, the latter is located in the folder *remotesync/target*. Here is an example of use with the main arguments : 

```bash
java -jar remotesync.jar -url=$URL_PIWIGO -usr=$USER_PIWIGO -pwd=$PWD_PIWIGO -dir=$FOLDER_TO_IMPORT -addext=$ADD_EXTENSION_FILES_TO_SYNCHRONISE -debug
```

To obtain the full list arguments, 

```bash
java -jar remotesync.jar -help
```

__Note__ : The maximum file size that can be synchronised may be limited by the php configuration. For larger files, it may be necessary to change the values of *upload_max_filesize* and *post_max_size* in the file *php.ini* of the server.
# Requirements

* A **Java** JRE/JDK (version **7** or greater)
* A **Piwigo** working **installation** (version **2.9** or greater)

# Quick start

1. **Download** the latest **remotesync-ui.jar** from https://github.com/Piwigo/Piwigo-Java/releases
2. **Double click** on it (your Operating System should use your default Java installation to execute the application)

# Screenshots

![Screenshot 1](http://sandbox.piwigo.com/uploads/4/y/1/4y1zzhnrnw//2019/07/19/20190719160350-ae92aa73.png)
![Screenshot 2](http://sandbox.piwigo.com/uploads/4/y/1/4y1zzhnrnw//2019/07/19/20190719160351-67c4095c.png)
![Screenshot 3](http://sandbox.piwigo.com/uploads/4/y/1/4y1zzhnrnw//2019/07/19/20190719160351-9e4aa111.png)
![Screenshot 4](http://sandbox.piwigo.com/uploads/4/y/1/4y1zzhnrnw//2019/07/19/20190719160743-7f8ba535.png)

# FAQ

* **What does "Sync" mean ?s**
* Right, it means "upload". This application will only upload new local images to your remote gallery

* **Is there any link to [Piwigo Import Tree](http://piwigo.org/doc/doku.php?id=user_documentation:tools:piwigo_import_tree) ?**
* Yes. As of 0.0.x, we try to offer the same level of functionality. Right now, we use the same cache mechanism (upload each local file once based on it's filename)   

# Troubleshooting

When you manage to upload some images, you may get some errors.
If so, please use the "Show log" button, and attach it to an issue here on Github.

# License

Piwigo-Java / Piwigo Remote Sync is released under the GPL v2 license.
